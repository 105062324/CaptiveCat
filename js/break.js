var breakState = {

    preload: function () {

        game.load.image("button1", "assets/menu/button1.png");
        game.load.image("button3", "assets/menu/button3.png");

    },

    create: function () {

        // 遊戲背景顏色
        game.stage.backgroundColor = "#222222";

        var TrashArray = [
            "萬事起頭難",
            "還不錯的開始?",
            "我知道你是怎麼破關的",
            "哼哼~ 遊戲開始",
            "人的意志力有多大?",
            "從努力到放棄",
            "想離開遊戲了嗎?",
            "死亡次數會使人感到挫折",
            "至少要再堅持10次",
            "說堅持幾次都是屁話",
            "歡樂的音樂會使你感到愉快",
            "和緩的音樂會使你感到平靜",
            "放過你的滑鼠",
            "加油  快結束了!",
            "努力不一定會成功",
            "放棄才是解決問題的方法",
            "最難的關卡還沒到呢!",
            "放棄吧 我會大聲的嘲笑你",
            "一關還有一關關",
            "無止盡的旅途使人灰心喪志",
        ];

        var trashTxt = TrashArray[ Math.floor(Math.random() * 20) ];

        if(game.global.level>=7) {
            var TXT = game.add.text(game.width / 2, 290, '恭喜你全破了!\n\n按Enter上傳紀錄到排行榜', { font: '40px Arial', fill: '#ffffff', align: "center" });
            TXT.anchor.setTo(0.5, 0.5);
        } else{
            var TXT = game.add.text(game.width / 2, 200, '\n' + trashTxt + '\n', { font: '40px Arial', fill: '#ffffff', align: "center" });
            TXT.anchor.setTo(0.5, 0.5);
        }
        

        if (game.global.level < 7) {
            // 開始按鈕
            this.button1 = game.add.sprite(game.width / 2, game.height - 100, 'button1')
            this.button1.scale.setTo(0.8, 0.5);
            this.button1.anchor.setTo(0.5, 0.5);
            game.add.tween(this.button1).to({ angle: -5 }, 1000).to({ angle: 5 }, 1000).to({ angle: 0 }, 1000).loop().start();

            // 點擊開始按鈕的動作
            this.button1.inputEnabled = true;
            this.button1.events.onInputDown.add((obj, pt) => {
                if(game.global.challenge)
                    game.state.start('playState');
                else
                    game.state.start('stageState');
            });
        }
        else {
            // 返回menu按鈕
            this.button3 = game.add.sprite(70, 530, 'button3')
            this.button3.scale.setTo(0.5, 0.5);
            this.button3.anchor.setTo(0.5, 0.5);
            game.add.tween(this.button3).to({ angle: -8 }, 1000).to({ angle: 8 }, 1000).to({ angle: 0 }, 1000).loop().start();

            // 點擊返回按鈕的動作
            this.button3.inputEnabled = true;
            this.button3.events.onInputDown.add((obj, pt) => {
                game.global.level = 1;
                game.global.died = 0;
                //
                game.global.challenge = 0;
                //
                game.state.start('menuState');
                
            });
        }
    },

    update: function () {

        if (game.global.level > 6) {

            /////////////////////////////////////////////
            // 全破後按Enter上傳死亡次數到排行榜
            var enterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
            enterKey.onDown.add(this.scoreRecord, this);
            ////////////////////////////////////////////
        }

    },

    // 上傳死亡次數到firebase

    scoreRecord: function () {
        var name = prompt("請輸入名字:", "name");
        firebase.database().ref('scores/').push({
            name: name,
            score: game.global.died,
        });
        var snapData = firebase.database().ref('scores/').orderByChild('score').limitToLast(6);
        var recordName = [];
        var recordScore = [];
        snapData.once('value', function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                recordName.push(childSnapshot.val().name);
                recordScore.push(childSnapshot.val().score);
            });
        }).then(function () {
            game.add.text(20, 20, 'Your Score has been uploaded!', { font: '30px Arial', fill: '#ffffff', backgroundColor: '	#220088' });
        });
    },


}