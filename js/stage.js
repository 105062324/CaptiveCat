function extendTime(n, count) {
    var str = n.toString();
    return "0".repeat(count - str.length) + str;
}

function getTimeString() {
    var t = new Date();
    return t.getFullYear() + "/" + extendTime(t.getMonth() + 1, 2) + "/" + extendTime(t.getDate(), 2) + " " + extendTime(t.getHours(), 2) + ":" + extendTime(t.getMinutes(), 2);
}

function shuffle(a) {
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
}




var stageState = {

    preload: function () {
        game.load.image("level", "assets/level.png");
        game.load.image("toolButton", "assets/tool.png");
        game.load.image("chanllengeButton", "assets/challenge.png");
        game.load.image("button3", "assets/menu/button3.png");
    },

    create: function () {

        const textPadding = 30;

        // 挑戰模式按鈕
        this.Cbutton = game.add.sprite(700,50, 'chanllengeButton')
        this.Cbutton.scale.setTo(0.5, 0.5);
        this.Cbutton.anchor.setTo(0.5, 0.5);
        game.add.tween(this.Cbutton).to({ angle: -8 }, 1000).to({ angle: 8 }, 1000).to({ angle: 0 }, 1000).loop().start();
        // 點擊開始按鈕的動作
        this.Cbutton.inputEnabled = true;
        this.Cbutton.events.onInputDown.add((obj, pt) => {
            if (this.levels.length < 7) {
                alert("Levels not enough QQ");
                return;
            }

            game.global.level = 1;
            game.global.died = 0;
            game.global.challenge = 1;
            game.global.content = [];
            $("#editor").css("visibility", "hidden");

            shuffle(Array.from({ length: this.levels.length }, (v, i) => i)).slice(0, 7).forEach((v, i) => {
                game.global.content.push(this.levels[v].Data.content);
            });

            game.state.start('breakState');
        });

        // 返回menu按鈕
        this.button3 = game.add.sprite(70, 530, 'button3')
        this.button3.scale.setTo(0.5, 0.5);
        this.button3.anchor.setTo(0.5, 0.5);
        game.add.tween(this.button3).to({ angle: -8 }, 1000).to({ angle: 8 }, 1000).to({ angle: 0 }, 1000).loop().start();

        // 點擊返回按鈕的動作
        this.button3.inputEnabled = true;
        this.button3.events.onInputDown.add((obj, pt) => {
            game.state.start('menuState');
        });





        // 遊戲背景顏色
        game.stage.backgroundColor = "#222222";

        this.levels = [];
        this.offset = 0;


        this.done = false;
        this.promise = new Promise((resolve, reject) => {
            firebase.database().ref("level").once("value").then((snapshot) => {
                var i = 0;
                snapshot.forEach(o => {
                    var box = game.add.sprite(0, 0, "level");
                    box.inputEnabled = true;
                    box.anchor.setTo(0.5, 0.5);

                    box.Data = o.val();
                    box.events.onInputDown.add((obj, pt) => {
                        game.global.content = box.Data.content;
                        game.state.start('playState');
                    });

                    var title = game.add.text(textPadding - box.width / 2, textPadding - box.height / 2, box.Data.name.substr(0, 22), { font: '30px Arial', fill: '#ffffff', align: "left" });
                    box.addChild(title);

                    var author = game.add.text(textPadding - box.width / 2, -textPadding + box.height / 2, box.Data.author.substr(0, 12), { font: '20px Arial', fill: '#ffffff', align: "left" });
                    author.anchor.setTo(0.0, 1.0);
                    box.addChild(author);

                    var date = game.add.text(- textPadding + box.width / 2, -textPadding + box.height / 2, box.Data.timestamp.substr(0, 10), { font: '20px Arial', fill: '#ffffff', align: "left" });
                    date.anchor.setTo(1.0, 1.0);
                    box.addChild(date);

                    this.levels.push(box);
                    i++;
                });

                resolve();
            });
        });

        game.input.mouse.mouseWheelCallback = (function (event) {
            var d;
            if (game.input.mouse.wheelDelta > 0) {
                d = -100;
            } else {
                d = 100;
            }

            (() => {
                const dt = 0.005;
                const T = 0.3;
                var t = 0;

                var id = window.setInterval(() => {
                    t += dt;
                    this.offset += Math.sin(Math.PI * t / T) * d * (dt / T);

                    if (t > T) {
                        window.clearInterval(id);
                    }
                }, dt * 1000);

            })();

        }).bind(this);

        this.button = game.add.sprite(800 - 20 - 160 * 0.4, 600 - 20 - 160 * 0.4, 'toolButton');
        this.button.scale.setTo(0.4, 0.4);
        this.button.inputEnabled = true;
        this.button.input.priorityID = 1;  // higher priority
        this.button.events.onInputDown.add((obj, pt) => {
            if ($("#editor").css("visibility") == "visible") {
                $("#editor").css("visibility", "hidden");
            } else {
                $("#editor").css("visibility", "visible");
            }
        });

    },

    update: function () {

        this.promise.then(() => {
            this.done = true;
        });


        if (!this.done) {
            return;
        }

        if (this.levels.length == 0) {
            let blank_level = {
                goal: [],
                wall: [],
                planet: [],
                playerInitialPosition: { x: 0, y: 590 }
            };

            alert("No level ! Entering editor mode");
            $("#editor").css("visibility", "visible");
            game.global.content = JSON.stringify(blank_level);
            game.state.start('playState');
        }

        this.offset = Math.min(this.levels.length * 180 - 600 + 180 - 120, this.offset);
        this.offset = Math.max(0, this.offset);
        this.levels.forEach((v, i) => {
            v.x = 400;
            v.y = 120 + 180 * i - this.offset;
        });

    }

};