var rankState = {

    preload: function () {

        game.load.image("rankTitle", "assets/menu/rankTitle.png");
        game.load.image("background", "assets/menu/bg.jpg");
        game.load.image("button3", "assets/menu/button3.png");

    },

    create: function () {


        // 遊戲背景sprite
        var background = game.add.sprite(0, 0, 'background');
        background.anchor.setTo(0, 0);
        game.add.tween(background.scale).to({ x: 1.09, y: 1.09 }, 6000).yoyo(true).loop().start();

        
        // 顯示排名標題
        var gameName = game.add.sprite(game.width / 2, 100, 'rankTitle');
        gameName.scale.setTo(1, 1.6);
        gameName.anchor.setTo(0.5, 0.5);
        

        // 返回menu按鈕
        this.button3 = game.add.sprite(70, 530, 'button3')
        this.button3.scale.setTo(0.5, 0.5);
        this.button3.anchor.setTo(0.5, 0.5);
        game.add.tween(this.button3).to({ angle: -8 }, 1000).to({ angle: 8 }, 1000).to({ angle: 0 }, 1000).loop().start();

        // 點擊返回按鈕的動作
        this.button3.inputEnabled = true;
        this.button3.events.onInputDown.add((obj, pt) => {
            game.state.start('menuState');
        });


        var rankList = game.add.text(game.width / 2, 240, '讀取中', { font: '35px Arial', fill: '#ffffff', align: "center" });
        rankList.anchor.setTo(0.5, 0.5);
        
        //************* 以下為Firebase讀取排名操作，若啟用記得把上面的tmp砍掉 *************// 
        
        var snapData = firebase.database().ref('scores/').orderByChild('score').limitToLast(6);
        var recordName = [];
        var recordScore = [];

        // 讀取排名歷史紀錄
        snapData.once('value', function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                recordName.push(childSnapshot.val().name);
                recordScore.push(childSnapshot.val().score);
            });
        }).then(function () {
            var rank = 1;
            var rankY = 220;
            for (var i = 0; i <= recordScore.length - 1; i++) {
                game.add.text(game.width / 2 - 130, rankY, 'Rank ' + rank + ' - ' + recordName[i] + ' - ' + recordScore[i], { font: '30px Arial', fill: '#ffffff', backgroundColor: '#000000', align: "center" });
                rank++;
                rankY += 50;
            }
            this.loadFinish = 1;
        });

    },

}
